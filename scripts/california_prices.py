import pandas as pd
import numpy as np
from sklearn.model_selection import train_test_split
import matplotlib.pyplot as plt

###IMPORTANDO O DADO
california_prices = pd.read_csv("data/housing.csv")

###GERANDO HISTOGRAMAS DAS VARIÁVEIS NUMÉRICAS
california_prices["income_cat"] = pd.cut(california_prices["median_income"],
    bins=[0., 1.5, 3.0, 4.5, 6., np.inf],
    labels=[1, 2, 3, 4, 5])


####LIMPANDO/TRATANDO O DATASET E GERANDO UM DATASET DE TESTE E TREINO BALANCEADO PELA CATEGORIA income_cat
california_prices["total_bedrooms"].fillna(california_prices["total_bedrooms"].median(), inplace = True)
california_prices.isnull().sum()

#Dummificando variável categórica
california_prices = pd.concat([california_prices.reset_index(drop=True),
                               pd.get_dummies(california_prices.ocean_proximity)], axis = 1)

california_prices = california_prices.drop("ocean_proximity", axis = 1)

california_prices['id'] = range(0, len(california_prices))

california_prices = california_prices.set_index('id', inplace = False)

from sklearn.model_selection import StratifiedShuffleSplit
split = StratifiedShuffleSplit(n_splits=2, test_size=0.2, random_state=42)

for train_index, test_index in split.split(california_prices, california_prices["income_cat"]):
    df_train = california_prices[california_prices.index.isin(train_index)]
    df_test = california_prices[california_prices.index.isin(test_index)]

#testando a distribuição da categoria de income_cat, para ver se estão parecidas
df_train["income_cat"].value_counts() / len(df_train)
df_test["income_cat"].value_counts() / len(df_test)

